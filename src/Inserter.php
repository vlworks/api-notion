<?php

namespace Vlproject\NotionMetric;

class Inserter
{
    protected const FIELD_PARENT = 'parent';
    protected const FIELD_PROPERTIES = 'properties';

    protected string $entityId;
    protected string $parentType = 'database_id';
    protected array $properties = [];
    protected array $body = [];



    public function __construct(string $entityId)
    {
        $this->entityId = $entityId;
    }



    protected function getParentParams(): array
    {
        return [
            'type' => $this->parentType,
            $this->parentType => $this->entityId
        ];
    }

    protected function getPropertiesParams(): array
    {
        return $this->properties;
    }

    public function getBody(): array
    {
        /** Parent Node */
        $this->body[self::FIELD_PARENT] = $this->getParentParams();

        /** Properties Node */
        $this->body[self::FIELD_PROPERTIES] = $this->getPropertiesParams();

        return $this->body;
    }

    /** Decorator  */

    public function add(string $fieldName, string $fieldType, array $entity): array
    {
        return $this->properties[$fieldName] = [
                $fieldType => [$entity]
            ];
    }
}


