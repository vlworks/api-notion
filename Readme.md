# VLWORKS API-Notion

Небольшое API для работы с Notion.

- Добавление новых записей(страниц) в рабочую базу по ID.

### Добавление новых записей(страниц) в рабочую базу по ID.

```php
$entityId = 'YOUR_ID' // default 'database_id'

$inserter = new Vlproject\NotionMetric\Inserter($entityId);

$inserter->add('Tags', 'multi_select', [ 'id' => 'ac4b0c40-42fc-4c0a-a179-76fa92414789' ]);
$inserter->add('HTTP_USER_AGENT', 'rich_text', [ 'text' => [ 'content' => 'HTTP_USER_AGENT' ] ]);
$inserter->add('HTTP_REFERER', 'rich_text', [ 'text' => [ 'content' => 'HTTP_REFERER' ], 'annotations' => [ "bold" => true ] ]);

$body = $inserter->getBody();

// ... some request client use $body
```

